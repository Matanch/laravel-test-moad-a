@extends('layouts.app')
@section('content')

<head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        </head>
     <!--Update-->

        <div class="container">
            <br/><br/>
            <h3>Edit customer profile</h3>
            
            <form method="post" action ="{{action('CustomerController@update' , $customers->id)}}">
                {{csrf_field()}}
                @method('PATCH')
                    <div class="form-group">
                        <label for="title"> Full name</label>
                        <input type ="text" class ="form-control" name="name" value = "{{$customers->name}}">
                    </div>


                    <div class="form-group">
                        <label for="title"> Email</label>
                        <input type ="email" class ="form-control" name="email" value = "{{$customers->email}}">
                    </div>

                     <div class="form-group">
                        <label for="title"> Phone number</label>
                        <input type ="number" class ="form-control" name="phone" value = "{{$customers->phone}}">
                    </div>
                   
                    <br>
                    <div class ="container">
                        <div class="col-4  offset-4">
                            <input type ="submit" class="form-control btn btn-secondary" name="submit" value =" save "> 
                        </div>
                    </div>

                    

                    <br>
                    <div class ="container">
                        <div class="col-4  offset-4">
                            <a href="{{route('customer.index')}}" class=" form-control btn btn-secondary">Back to customers list</a>
                         </div>
                    </div>
            </form>
        </div>

       
        
        @endsection