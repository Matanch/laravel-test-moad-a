@extends('layouts.app')
@section('content')

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class='container'>
<h1>Customers List </h1>
    <br>


    <div class ="container">
        <div class="col-0  offset-0">
           <a href="{{route('customer.create')}}" class=" btn btn-secondary">create new customers</a>
       </div>

       <br>
      <table class="table table-bordered">

          <thead class="thead-dark">
            <tr>
              <th scope="col">Customer Name</th>
              <th scope="col">Email</th>
              <th scope="col">Phone Number</th>
              <th scope="col">Status</th>
              <th scope="col">Edit </th>
              <th scope="col">Delete </th>
            </tr>
          </thead>
      
         <div class="alert alert-primary" role="alert">
         To close the deal click on the link "prospect"
        </div>

          <tbody>
          @foreach($customers as $customer)
            <tr>
                <td>{{$customer->name}}</td>
                <td>{{$customer->email}}</td>
                <td>{{$customer->phone}}</td>
                <td>
                @if($customer->status == 0)
                <a href = "{{route('done' , $customer->id)}}">prospect</a>

                @else
                <div class="alert alert-success" role="alert">deal closed</div>
                @endif

                </td>
                <td><a href = "{{route('customer.edit' , $customer->id)}}">Edit customer</td>
                
                @method('DELETE')
                @can('manager')
                <td><a href = "{{route('delete', $customer->id)}}" class=" btn btn-secondary">Delete</td>
                @endcan

                @cannot('manager')          
                <td>    Delete</td>

                @endcannot

            </tr>
            @endforeach
          </tbody>
      </table>

      
      @endsection