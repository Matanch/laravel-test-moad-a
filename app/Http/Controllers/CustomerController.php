<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Collection;
use App\Customer;
use App\User;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view('customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        $customers = new Customer();
        $id = Auth::id(); //the idea of the current user
        $customers->name = $request->name;
        $customers->email = $request->email;
        $customers->phone = $request->phone;
        $customers->status = 0;
        $customers->status = 'prospect';
        $customers->user_id = $id;
        $customers->save();
        return redirect('customer');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $customers = Customer::findOrFail($id);
        return view('customer.edit', compact('customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customers = Customer::findOrFail($id);            
        $customers->update($request->except(['_token'])); 
        return redirect('customer');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to delete customers..");
        }
 
        $customers = Customer::find($id);
        $customers->delete();
        return redirect('customer');
    }

    public function done($id)
    {
        //only if this todo belongs to user 
        if (Gate::denies('manager')) {
            abort(403,"You are not allowed to change status..");
         }          
        $customers = Customer::findOrFail($id);            
        $customers->status = 1; 
        $customers->save();
        return redirect('customer');    
    }    

    // public function indexFiltered()
    // {
    //     //$id = Auth::id(); //the current user 
    //     //$customers = Customer::where('user_id', $id)->get();
    //     $id = User::all('user_id');
    //     $user_id = Customer::all('user_id');
    //     if ($id != $user_id) {
    //         abort(403,"Sorry, you do not hold permission to edit this customer");
    //      }   
    //      return view('customer.edit');
    //     }    
}
