<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;
use App\User;

class Customer extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

     protected$fillable =[
         'name','email','phone',
     ];
}
