<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert(
            [
                [
                    'name' => 'Ori Hakak',
                    'user_id' => 1,
                    'email' => 'orihk@gmail.com',
                    'phone' =>('44444444'),
                    'status' => 'prospect',
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'Avi chover',
                    'user_id' => 2,
                    'email' => 'avi@gmail.com',
                    'phone' =>('66666666'),
                    'status' => 'customer',
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'Eyal Keren',
                    'user_id' => 3,
                    'email' => 'eyal@gmail.com',
                    'phone' =>('5555555'),
                    'status' => 'customer',
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                ],
            ]
        );
       
    }
    
}
