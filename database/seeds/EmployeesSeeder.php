<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert(
            [
                [
                    'manager' => '1',
                    'customer' => '2',
                    'created_at' => date('Y-m-d G:i:s')
                    ],
                   
            ]
                );
    }
}
