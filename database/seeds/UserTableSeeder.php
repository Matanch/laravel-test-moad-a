<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Matan Chover',
                    'user_id' => 1,
                    'email' => 'a@a.com',
                    'password' =>Hash::make('12345678'),
                    'role' => 'manager',
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'Roni Horowitz',
                    'user_id' => 2,
                    'email' => 'b@b.com',
                    'password' =>Hash::make('12345678'),
                    'role' => 'salesrep',
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'user_id' => 3,
                    'name' => 'Amir mesillati',
                    'user_id' => 3,
                    'email' => 'c@c.com',
                    'password' =>Hash::make('12345678'),
                    'role' => 'salesrep',
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                ],
            ]
        );
       
    }
    
}
